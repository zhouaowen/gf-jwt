module gitee.com/zhouaowen/gf-jwt/v2

go 1.15

require (
	github.com/gogf/gf/v2 v2.0.0-rc3
	gitee.com/zhouaowen/jwt/v4 v4.5.1
)
